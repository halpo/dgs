//
//  Image.h
//  Dead Good Shit
//
//  Created by Paul on 10/04/2014.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Image : NSManagedObject

@property (nonatomic, retain) NSData *data;
@property (nonatomic, retain) NSDate *postDate;
@property (nonatomic, retain) NSString *url;
@property (nonatomic, retain) NSString *postId;
@property (nonatomic, retain) NSString *tags;

@end
