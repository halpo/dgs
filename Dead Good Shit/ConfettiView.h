//
//  ConfettiView.h
//  Dead Good Shit
//
//  Created by Paul on 16/04/2014.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConfettiView : UIView
- (void) startEmitting;
- (void) stopEmitting;
- (void) decayStep;
- (void) decayOverTime:(NSTimeInterval)interval;
@end
