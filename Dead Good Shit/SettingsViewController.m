//
//  SettingsViewController.m
//  Dead Good Shit
//
//  Created by Paul on 10/04/2014.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import "SettingsViewController.h"
#import "TableViewController.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void)viewDidLoad {
	[super viewDidLoad];

	float w = [[UIScreen mainScreen] bounds].size.width;
	float h = [[UIScreen mainScreen] bounds].size.height;
	float buttonWidth = 250;
	float buttonHeight = 40;

	CGRect pickerFrame = CGRectMake(w * 0.5 - 150, (h - kNavHeight) * 0.5 - 170 + kNavHeight, 300, 340);

	UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Light" size:18.0f];

	NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"theme"];
	UIColor *themeColor = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];

	UIButton *webButton = [UIButton buttonWithType:UIButtonTypeCustom];
	webButton.frame = CGRectMake((w * 0.5) - (buttonWidth * 0.5), (pickerFrame.origin.y - kNavHeight) * 0.5 - buttonHeight * 0.5 + kNavHeight, buttonWidth, buttonHeight);
	[webButton setTitle:@"www.DeadGoodShit.com" forState:UIControlStateNormal];
	[webButton setBackgroundColor:[UIColor redColor]];
	[webButton.titleLabel setFont:font];
	[webButton setBackgroundColor:themeColor];
	[webButton addTarget:self action:@selector(website) forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:webButton];

	UIButton *deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
	deleteButton.frame = CGRectMake((w * 0.5) - (buttonWidth * 0.5), h - ((h - kNavHeight - 340) * 0.25 + buttonHeight * 0.5), buttonWidth, buttonHeight);
	[deleteButton setTitle:@"DELETE ALL DATA" forState:UIControlStateNormal];
	[deleteButton setBackgroundColor:[UIColor redColor]];
	[deleteButton.titleLabel setFont:font];
	[deleteButton addTarget:self action:@selector(erase) forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:deleteButton];
	self.view.backgroundColor = [UIColor blackColor];

	// Color did change block declaration
	NKOColorPickerDidChangeColorBlock colorDidChangeBlock = ^(UIColor *color) {
		NSData *colorData = [NSKeyedArchiver archivedDataWithRootObject:color];
		[[NSUserDefaults standardUserDefaults] setObject:colorData forKey:@"theme"];
		[[NSUserDefaults standardUserDefaults] synchronize];
		[[UINavigationBar appearance] setBarTintColor:color];
		[webButton setBackgroundColor:color];
		UIViewController *vc = [[UIViewController alloc]init];
		[self presentViewController:vc animated:NO completion:nil];
		[vc dismissViewControllerAnimated:NO completion:nil];
	};

	NKOColorPickerView *colorPickerView = [[NKOColorPickerView alloc] initWithFrame:pickerFrame color:themeColor andDidChangeColorBlock:colorDidChangeBlock];

	// Add color picker to your view
	[self.view addSubview:colorPickerView];

	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

- (void)website {
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://deadgoodshit.com/"]];
}

- (void)erase {
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Delete all data" message:@"All downloaded images will be erased" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];

	[alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (buttonIndex == 1) {
		MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
		hud.labelText = @"Deleting...";
		NSArray *images = [Image MR_findAll];
		for (Image *im in images) {
			[[SDImageCache sharedImageCache] removeImageForKey:im.url fromDisk:YES];
		}
		images = [SmallImage MR_findAll];
		for (SmallImage *im in images) {
			[[SDImageCache sharedImageCache] removeImageForKey:im.url fromDisk:YES];
		}
		[Image MR_truncateAll];
		[SmallImage MR_truncateAll];
		[[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion: ^(BOOL success, NSError *error) {
		    [hud hide:YES];
		}];
	}
}

@end
