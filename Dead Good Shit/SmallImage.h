//
//  SmallImage.h
//  Dead Good Shit
//
//  Created by Paul on 08/04/2014.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface SmallImage : NSManagedObject

@property (nonatomic, retain) NSData *data;
@property (nonatomic, retain) NSString *url;
@property (nonatomic, retain) NSDate *postDate;

@end
