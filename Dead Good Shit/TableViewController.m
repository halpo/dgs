//
//  TableViewController.m
//  Dead Good Shit
//
//  Created by Paul on 08/04/2014.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import "TableViewController.h"
#import "ConfettiView.h"

#define kXMLURL @"http://deadgoodshit.tumblr.com/api/read/?num=50"
#define kEasterCode    @"02120" // For Easter Egg (Order to swipe rows)
#define iPad UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad

@interface TableViewController () {
	NSMutableArray *imageDicts;
	NSMutableArray *easterPass;
	NSString *codeString;
	SDWebImageManager *manager;
	int kIvWidth;
	int kIvHeight;
	UIColor *themeColor;
	MBProgressHUD *hud;
	UIImageView *zoomIv;
	UIButton *navBut;
	ConfettiView *con;
	AVAudioPlayer *party;
	UIPopoverController *pop;
}

@end

@implementation TableViewController

#pragma mark Init

- (id)initWithStyle:(UITableViewStyle)style {
	self = [super initWithStyle:style];
	if (self) {
	}
	return self;
}

- (void)viewWillAppear:(BOOL)animated {
	// Refreshes theme etc
	[self viewDidLoad];
}

- (void)viewDidLoad {
	[super viewDidLoad];


    
	// Easter egg init
	codeString = kEasterCode;
	easterPass = [NSMutableArray new];

	// ipad / iphone screen size
	if (iPad) {
		kIvWidth = 1536;
		kIvHeight = 200;
	}
	else {
		kIvWidth = 640;
		kIvHeight = 200;
	}

	self.title = @"DEAD GOOD SHIT";

	//Nav Tap
	if (!navBut) {
		navBut = [UIButton buttonWithType:UIButtonTypeCustom];
		int buttonWidth = 180;
		navBut.frame = CGRectMake((kIvWidth * 0.25) - (buttonWidth * 0.5), 0, buttonWidth, 44);
		[navBut addTarget:self action:@selector(navTap) forControlEvents:UIControlEventTouchUpInside];
		[self.navigationController.navigationBar addSubview:navBut];
	}

	// Get theme color
	NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"theme"];
	themeColor = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];

	self.view.backgroundColor = themeColor;

	[self fetch];

	if (imageDicts) {
		[self.tableView reloadData];
	}
	else {
		// No connection on first-time run
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Internet Connection" message:@"Requires internet connection to initialise" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alert show];
	}

	//Pull to refresh
	NSAttributedString *aStr = [[NSAttributedString alloc] initWithString:@"LOADING POSTS..." attributes:@{ NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Light" size:14.0], NSForegroundColorAttributeName:[UIColor whiteColor] }];
	if (!self.refreshControl) {
		UIRefreshControl *rc = [UIRefreshControl new];
		rc.tintColor = [UIColor whiteColor];
		[rc addTarget:self action:@selector(reload) forControlEvents:UIControlEventValueChanged];
		rc.attributedTitle = aStr;
		rc.backgroundColor = themeColor;
		self.refreshControl = rc;
	}
	else {
		//self.refreshControl.tintColor = themeColor;
		self.refreshControl.backgroundColor = themeColor;
		self.refreshControl.attributedTitle = aStr;
	}
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return imageDicts.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	// custom cell (SWTableViewCell)
	MyCuteCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ImageCell" forIndexPath:indexPath];

	NSMutableArray *rightUtilityButtons = [NSMutableArray new];

	// Delete and Share buttons
	[rightUtilityButtons sw_addUtilityButtonWithColor:themeColor icon:[UIImage imageNamed:@"Share"]];
	[rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:1.0f green:0.231f blue:0.188 alpha:1.0f] icon:[UIImage imageNamed:@"Cross"]];

	//Static cell setup
	MyCuteCell __weak *weakCell = cell;
	[cell setAppearanceWithBlock: ^{
	    weakCell.leftUtilityButtons = nil;
	    weakCell.rightUtilityButtons = rightUtilityButtons;
	    weakCell.delegate = self;
	    weakCell.containingTableView = tableView;
	    weakCell.label.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:18.0f];
	    weakCell.label2.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:12.0f];
	    weakCell.pv.tintColor = [UIColor whiteColor];
	    weakCell.iv.contentMode = UIViewContentModeScaleAspectFill;
	    weakCell.clipsToBounds = YES;
	} force:NO];

	// Dynamic cell setup
	[cell setCellHeight:cell.frame.size.height];
	UIView *selectionColor = [[UIView alloc] init];
	selectionColor.backgroundColor = themeColor;
	[cell.label setTextColor:themeColor];
	cell.pv.hidden = YES;
	cell.selectedBackgroundView = selectionColor;
	cell.backgroundColor = themeColor;

	SmallImage *t = [SmallImage MR_findFirstByAttribute:@"url" withValue:[[imageDicts objectAtIndex:indexPath.row] valueForKey:@"url"]];
	if (t) {
		// display image if present
		[cell.iv setImage:[UIImage imageWithData:t.data]];
		cell.label.hidden = YES;
		cell.label2.hidden = YES;
		cell.rightUtilityButtons = rightUtilityButtons;
	}
	else {
		// Cell setup - no image
		[cell.iv setImage:[UIImage imageWithData:nil]];
		cell.iv.backgroundColor = themeColor;
		NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
		[dateFormat setDateFormat:@"dd-MM-yyyy"];
		NSString *date = [dateFormat stringFromDate:[[imageDicts objectAtIndex:indexPath.row] valueForKey:@"date"]];
		cell.label.text = [NSString stringWithFormat:@"%@", date];
		cell.label2.text = @"TAP TO LOAD";
		cell.label.hidden = NO;
		cell.label2.hidden = NO;
		cell.label.textColor = [UIColor whiteColor];
		cell.rightUtilityButtons = nil;
	}

	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	// deselect row
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	MyCuteCell *cell = (MyCuteCell *)[self.tableView cellForRowAtIndexPath:indexPath];
	// check to see if image exists @ location
	SmallImage *t = [SmallImage MR_findFirstByAttribute:@"url" withValue:[[imageDicts objectAtIndex:indexPath.row] valueForKey:@"url"]];
	if (!t && !manager.isRunning && imageDicts) {
		// no interaction while loading

		cell.userInteractionEnabled = NO;

		// Fetch image data
		manager = [SDWebImageManager sharedManager];
		[manager downloadWithURL:[[imageDicts objectAtIndex:indexPath.row] valueForKey:@"url"]
		                 options:0
		                progress: ^(NSInteger receivedSize, NSInteger expectedSize)
		{
		    // Loading block
		    int t = 0;
		    if (t == 0) {
		        [cell bringSubviewToFront:cell.pv];
		        cell.pv.hidden = NO;
		        cell.label2.hidden = YES;
		        [self.navigationController.navigationBar setUserInteractionEnabled:NO];
			}
		    t++;
		    float progress = (float)receivedSize / (float)expectedSize;
		    progress = sqrtf(progress * progress);
		    cell.label.text = [NSString stringWithFormat:@"%.2f%%", progress * 100.0];
		    cell.pv.progress = progress;
		    if (progress > 0.95) {
		        cell.pv.hidden = YES;
		        cell.label.hidden = NO;
		        cell.label2.hidden = YES;
		        cell.label.text = @"Saving...";
			}
		}

		               completed: ^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished)
		{
		    if (image && finished) {
		        // Completion block

		        float pix = (float)image.size.width * (float)image.size.height;
		        UIImage *reduced = nil;
		        if (pix > 2000000.0) {
		            // Resize image if too large
		            float red = sqrtf(1200000.0 / pix);
		            CGFloat ww = floor(red * image.size.width);
		            CGFloat hh = floor(red * image.size.height);
		            reduced = [self resizeImage:image width:ww height:hh shouldCrop:NO];
				}

		        if (reduced) {
		            image = reduced;
				}

		        // High res image
		        Image *x = [Image MR_createEntity];
		        x.url = [[imageDicts objectAtIndex:indexPath.row] valueForKey:@"url"];
		        x.postDate = [[imageDicts objectAtIndex:indexPath.row] valueForKey:@"date"];
		        x.postId = [[imageDicts objectAtIndex:indexPath.row] valueForKey:@"id"];
		        x.tags = [[imageDicts objectAtIndex:indexPath.row] valueForKey:@"tags"];

		        // Create small/cropped image for tableView
		        SmallImage *y = [SmallImage MR_createEntity];
		        y.url = [[imageDicts objectAtIndex:indexPath.row] valueForKey:@"url"];
		        y.postDate = [[imageDicts objectAtIndex:indexPath.row] valueForKey:@"date"];
		        UIImage *smallImage = [self resizeImage:image width:kIvWidth height:((float)image.size.height / (float)image.size.width) * (float)kIvWidth shouldCrop:YES];

		        NSString *fileType = [[imageDicts objectAtIndex:indexPath.row] valueForKey:@"url"];
		        fileType = [fileType pathExtension].lowercaseString;

		        if ([fileType isEqualToString:@"png"]) {
		            x.data = UIImagePNGRepresentation(image);
		            y.data = UIImagePNGRepresentation(smallImage);
				}
		        else if ([fileType isEqualToString:@"jpg"] || [fileType isEqualToString:@"jpeg"]) {
		            x.data = UIImageJPEGRepresentation(image, 0.8);
		            y.data = UIImageJPEGRepresentation(smallImage, 0.8);
				}
		        else {
		            x.data = UIImageJPEGRepresentation(image, 0.8);
		            y.data = UIImageJPEGRepresentation(smallImage, 0.8);
				}

		        [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion: ^(BOOL success, NSError *error) {
		            if (success) {
		                // Download success
		                // clear cache and tableviewcell
		                [[SDImageCache sharedImageCache] removeImageForKey:[[imageDicts objectAtIndex:indexPath.row] valueForKey:@"url"] fromDisk:YES];
		                cell.label.hidden = YES;
		                cell.label2.hidden = YES;
		                cell.label.text = @"";
		                cell.label2.text = @"";
		                cell.pv.hidden = YES;
		                cell.userInteractionEnabled = YES;
		                [self.navigationController.navigationBar setUserInteractionEnabled:YES];
					}
				}];

		        [self.tableView reloadData];
			}
		    else {
		        // Download failed
		        // clear any data
		        cell.label.hidden = NO;
		        cell.label2.hidden = NO;
		        cell.label.text = [NSString stringWithFormat:@"Download Failed"];
		        cell.label2.text = [NSString stringWithFormat:@"%@", error];
		        cell.pv.hidden = YES;
		        cell.userInteractionEnabled = YES;
		        [self.navigationController.navigationBar setUserInteractionEnabled:YES];
		        [self deleteEntry:indexPath];
			}
		}];
	}
	else if (t && imageDicts) {
		// display photo viewer
		[self photoViewerWithImage:[Image MR_findFirstByAttribute:@"postId" withValue:[[imageDicts objectAtIndex:indexPath.row] valueForKey:@"id"]]];
	}
}

- (void)fetch {
	BOOL shouldFetch = [[NSUserDefaults standardUserDefaults] boolForKey:@"shouldFetch"];
	if (shouldFetch) {
		// Get XML from tumblr
		NSError *error;
		NSString *xmlString = [NSString stringWithContentsOfURL:[NSURL URLWithString:kXMLURL] encoding:NSUTF8StringEncoding error:&error];

		if (xmlString) {
			// Parse Posts

			[[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"shouldFetch"];
			[[NSUserDefaults standardUserDefaults] synchronize];

			NSData *xmlData = [xmlString dataUsingEncoding:NSUTF8StringEncoding];

			imageDicts = [NSMutableArray new];
			RXMLElement *rootXML = [RXMLElement elementFromXMLData:xmlData];

			[rootXML iterate:@"posts.post" usingBlock: ^(RXMLElement *e) {
			    NSString *imageUrl = [NSString stringWithFormat:@"%@", [e child:@"regular-body"].text];
			    if ([imageUrl rangeOfString:@"src="].location != NSNotFound) {
			        imageUrl = [imageUrl substringFromIndex:[imageUrl rangeOfString:@"src="].location + 5];
			        NSArray *ta = [imageUrl componentsSeparatedByString:@"\""];
			        imageUrl = [ta objectAtIndex:0];
			        NSString *dateString = [e attribute:@"date"];
			        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
			        [dateFormat setDateFormat:@"EEE, d LLL yyyy kk:mm:ss"];
			        NSDate *date = [dateFormat dateFromString:dateString];
			        NSString *postId = [e attribute:@"id"];
			        NSMutableString *tags = [NSMutableString new];
			        [e iterate:@"tag" usingBlock: ^(RXMLElement *t) {
			            [tags appendString:[NSString stringWithFormat:@"%@|", t.text]];
					}];
			        tags = (NSMutableString *)[tags substringToIndex:tags.length - 1];

			        // Save post dictionary in array
			        NSDictionary *dict = [NSDictionary dictionaryWithObjects:@[imageUrl, date, postId, tags] forKeys:@[@"url", @"date", @"id", @"tags"]];
			        [imageDicts addObject:dict];
				}
			}];

			[[NSUserDefaults standardUserDefaults] setObject:imageDicts forKey:@"imageDicts"];
			[[NSUserDefaults standardUserDefaults] synchronize];
		}
	}
	else {
		// get old links if we are not fetching / offline
		imageDicts = [[NSUserDefaults standardUserDefaults] objectForKey:@"imageDicts"];
	}
	[self.refreshControl endRefreshing];
	[self.tableView reloadData];
}

#pragma mark SWTableView Delegate

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index {
	NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];

	[cell hideUtilityButtonsAnimated:YES];
	switch (index) {
		case 0: // Share
			[self share:indexPath];
			break;

		case 1: { // Delete
			SmallImage *t = [SmallImage MR_findFirstByAttribute:@"url" withValue:[[imageDicts objectAtIndex:indexPath.row] valueForKey:@"url"]];
			if (t) {
				[self deleteEntry:indexPath];
			}
		}
		break;

		default:
			break;
	}
}

- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell {
	// Returns swiped cells to center when activated
	return YES;
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell scrollingToState:(SWCellState)state {
	// Easter Egg Code
	if (state == kCellStateRight) {
		[easterPass addObject:[NSString stringWithFormat:@"%ld", (long)[self.tableView indexPathForCell:cell].row]];
		if (easterPass.count > codeString.length) {
			[easterPass removeObjectAtIndex:0];
		}
		NSString *ee = [easterPass componentsJoinedByString:@""];
		if ([ee isEqualToString:codeString]) {
			[self easter];
		}
	}
}

#pragma mark - Misc

- (void)photoViewerWithImage:(Image *)image {
	// Display photo Viewer
	zoomIv = [UIImageView new];
	zoomIv.image = [UIImage imageWithData:image.data];
	[EXPhotoViewer showImageFrom:zoomIv];
}

- (UIImage *)resizeImage:(UIImage *)image width:(CGFloat)resizedWidth height:(CGFloat)resizedHeight shouldCrop:(BOOL)crop {
	CGImageRef imageRef = [image CGImage];
	CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
	CGContextRef bitmap = CGBitmapContextCreate(NULL, resizedWidth, resizedHeight, 8, 4 * resizedWidth, colorSpace, (CGBitmapInfo)kCGImageAlphaPremultipliedFirst);

	CGContextDrawImage(bitmap, CGRectMake(0, 0, resizedWidth, resizedHeight), imageRef);
	CGImageRef ref = CGBitmapContextCreateImage(bitmap);
	UIImage *result;

	if (crop) { // for SmallImage (tableviewcell)
		CGRect cropRect = CGRectMake(0, CGImageGetHeight(ref) * 0.5 - ((float)kIvHeight * 0.5), (float)kIvWidth, (float)kIvHeight);
		CGImageRef cropRef = CGImageCreateWithImageInRect(ref, cropRect);
		result = [UIImage imageWithCGImage:cropRef];
		CGImageRelease(cropRef);
	}
	else {
		result = [UIImage imageWithCGImage:ref];
	}

	CGImageRelease(ref);
	CGContextRelease(bitmap);
	return result;
}

- (void)deleteEntry:(NSIndexPath *)indexPath {
	hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
	hud.labelText = @"Deleting...";
	SmallImage *t = [SmallImage MR_findFirstByAttribute:@"url" withValue:[[imageDicts objectAtIndex:indexPath.row] valueForKey:@"url"]];
	Image *ts = [Image MR_findFirstByAttribute:@"postId" withValue:[[imageDicts objectAtIndex:indexPath.row] valueForKey:@"id"]];
	[[SDImageCache sharedImageCache] removeImageForKey:[[imageDicts objectAtIndex:indexPath.row] valueForKey:@"url"] fromDisk:YES];
	[t MR_deleteEntity];
	[ts MR_deleteEntity];
	[[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion: ^(BOOL success, NSError *error) {
	    if (success) {
	        [hud hide:YES];
	        [self viewDidLoad];
		}
	    else {
	        [hud hide:YES];
		}
	}];
}

- (void)reload {
	// force fetch
	[[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"shouldFetch"];
	[[NSUserDefaults standardUserDefaults] synchronize];
	[self fetch];
}

- (void)share:(NSIndexPath *)indexPath {
	// Social Share
	Image *theImage = [Image MR_findFirstByAttribute:@"postId" withValue:[[imageDicts objectAtIndex:indexPath.row] valueForKeyPath:@"id"]];

	if (theImage) {
		UIImage *image = [UIImage imageWithData:theImage.data];
		NSArray *activityItems;
		NSString *activityText = @"What a nice image!\nwww.DeadGoodShit.com";
		activityItems = @[activityText, image];
		UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];

		if (!iPad) {
			[self presentViewController:activityController animated:YES completion:nil];
		}
		else {
			pop = [[UIPopoverController alloc] initWithContentViewController:activityController];
			[pop presentPopoverFromRect:[self.tableView rectForRowAtIndexPath:indexPath] inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
		}
	}
}

- (void)easter {
	self.tableView.userInteractionEnabled = NO;
	self.navigationController.navigationBar.userInteractionEnabled = NO;  // disable interaction

	CGRect tableFrame = self.view.frame;
	CGRect tableGone = CGRectOffset(tableFrame, 0, tableFrame.size.height);

	zoomIv = [[UIImageView alloc] initWithFrame:tableGone];  // create imageview
	zoomIv.contentMode = UIViewContentModeScaleAspectFit;
	zoomIv.image = [UIImage imageNamed:@"Test"];
	[self.view addSubview:zoomIv];
	zoomIv.transform = CGAffineTransformMakeRotation(M_PI - 0.001); // upside-down

	if (!con) { //Confetti
		con = [[ConfettiView alloc] initWithFrame:self.view.bounds];
		[self.view addSubview:con];
		[con decayOverTime:5];
	}
	else {
		[self.view bringSubviewToFront:con];
		[con startEmitting];
		[con decayOverTime:5];
	}

	if (!party) {  //Party blower
		NSURL *partyFile = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"blow" ofType:@"wav"]];
		party = [[AVAudioPlayer alloc] initWithContentsOfURL:partyFile error:nil];
		party.volume = 0.8;
	}
	[party prepareToPlay];
	[party play];

	NSString *string = @"You cheeky little bugger!!";   //Voice message
	AVSpeechUtterance *utterance = [[AVSpeechUtterance alloc] initWithString:string];
	utterance.voice = [AVSpeechSynthesisVoice voiceWithLanguage:@"en-ZA"];
	utterance.rate = 0.3;
	utterance.pitchMultiplier = 1.7;
	utterance.volume = 0.8;
	AVSpeechSynthesizer *speechSynthesizer = [[AVSpeechSynthesizer alloc] init];

	[UIView animateWithDuration:2.0 animations: ^{
	    zoomIv.frame = tableFrame; // slide in
	    zoomIv.transform = CGAffineTransformMakeRotation(0);
	} completion: ^(BOOL finished) {
	    if (finished) {
	        [speechSynthesizer speakUtterance:utterance];  //speak
	        zoomIv.image = [UIImage imageNamed:@"Wink"]; // wink
	        [UIView animateWithDuration:1.5 delay:1.7 options:UIViewAnimationOptionCurveLinear animations: ^{
	            zoomIv.frame = tableGone; // slide out
	            zoomIv.transform = CGAffineTransformMakeRotation(M_PI);
			} completion: ^(BOOL finished) {
	            if (finished) {
	                [zoomIv removeFromSuperview]; // remove & re-enable interaction
	                self.tableView.userInteractionEnabled = YES;
	                self.navigationController.navigationBar.userInteractionEnabled = YES;
				}
			}];
		}
	}];
}

- (void)navTap {
	[self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
}

@end
