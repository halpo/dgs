//
//  AppDelegate.m
//  Dead Good Shit
//
//  Created by Paul on 08/04/2014.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
	[[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"shouldFetch"];
	NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"theme"];
	UIColor *themeColor;
	if (colorData) {
		themeColor = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
	}
	if (!themeColor) {
		themeColor = [UIColor colorWithRed:0.086720 green:0.513053 blue:0.532143 alpha:1.0];
		NSData *colorData = [NSKeyedArchiver archivedDataWithRootObject:themeColor];
		[[NSUserDefaults standardUserDefaults] setObject:colorData forKey:@"theme"];
	}
	[[NSUserDefaults standardUserDefaults] synchronize];
	[[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
	[[UINavigationBar appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName: [UIFont fontWithName:@"Helvetica" size:20.0f] }];
	[[UINavigationBar appearance] setBarTintColor:themeColor];
	[MagicalRecord setupCoreDataStackWithStoreNamed:@"data"];


	return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
	// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
	// Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
	// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
	// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
	[[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"shouldFetch"];
	[[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
	// Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
	// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
	// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
	NSArray *images = [Image MR_findAll];

	for (Image *im in images) {
		[[SDImageCache sharedImageCache] removeImageForKey:im.url fromDisk:YES];
	}
	images = [SmallImage MR_findAll];
	for (SmallImage *im in images) {
		[[SDImageCache sharedImageCache] removeImageForKey:im.url fromDisk:YES];
	}
	[[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"shouldFetch"];
	[[NSUserDefaults standardUserDefaults] synchronize];
	[MagicalRecord cleanUp];
}

@end
